# -*- coding: utf-8 -*-
"""
Game of life script with animated evolution

Created on Tue Jan 15 12:37:52 2019

@author: shakes
"""
import conway

if __name__ == '__main__':
    # for 3-engine cordership gun
    # N = 300

    # for new gun
    N = 70

    # create the game of life object
    life = conway.GameOfLife(N)
    # life.insertBlinker((0,0))

    # life.insertGliderGun((0, 0))
    cells = life.getStates()  # initial state

    with open("B-52 bomber.txt", "r") as text_file:
        txtString = text_file.read()
        life.insertFromPlainText(txtString)

    # -------------------------------
    # plot cells
    import matplotlib.pyplot as plt
    import matplotlib.animation as animation

    fig = plt.figure()

    plt.gray()

    img = plt.imshow(cells, animated=True)


    def animate(i):
        """perform animation step"""
        global life

        life.evolve()
        cellsUpdated = life.getStates()

        img.set_array(cellsUpdated)

        return img,


    interval = 20  # ms

    # animate 24 frames with interval between them calling animate function at each frame
    ani = animation.FuncAnimation(fig, animate, frames=24, interval=interval, blit=True)

    plt.show()

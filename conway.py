# -*- coding: utf-8 -*-
"""
The Game of Life (GoL) module named in honour of John Conway

This module defines the classes required for the GoL simulation.

Created on Tue Jan 15 12:21:17 2019

@author: shakes
"""
import numpy as np
from scipy import signal

import rle


class GameOfLife:
    '''
    Object for computing Conway's Game of Life (GoL) cellular machine/automata
    '''

    def __init__(self, N=256, finite=False, fastMode=False):
        self.grid = np.zeros((N, N), np.int)
        self.neighborhood = np.ones((3, 3), np.int)  # 8 connected kernel
        self.neighborhood[1, 1] = 0  # do not count centre pixel
        self.finite = finite
        self.fastMode = fastMode
        self.aliveValue = 1
        self.deadValue = 0
        self.N = N

    def getStates(self):
        '''
        Returns the current states of the cells
        '''
        return self.grid

    def getGrid(self):
        '''
        Same as getStates()
        '''
        return self.getStates()

    def evolve(self):
        '''
        Given the current states of the cells, apply the GoL rules:
        - Any live cell with fewer than two live neighbors dies, as if by underpopulation.
        - Any live cell with two or three live neighbors lives on to the next generation.
        - Any live cell with more than three live neighbors dies, as if by overpopulation.
        - Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction
        '''

        # get weighted sum of neighbors
        # PART A & E CODE HERE

        # PART A
        """
        new_grid = np.zeros((self.N, self.N), np.int)
        
        for i in range(self.N):
            for j in range(self.N):
                if i == 0 and j == 0:
                    # check for the most top left cell
                    neighborhoodSum = self.grid[i][j + 1] + self.grid[i + 1][j] + self.grid[i + 1][j + 1]

                elif i == 0 and j == self.N - 1:
                    # check for the most top right cell
                    neighborhoodSum = self.grid[i][j - 1] + self.grid[i + 1][j] + self.grid[i + 1][j - 1]

                elif i == 0 and self.N - 1 > j > 0:
                    # check for the top row of cells except from the most right and left cells
                    neighborhoodSum = self.grid[i][j - 1] + self.grid[i][j + 1] + self.grid[i + 1][j - 1] + \
                                      self.grid[i + 1][j + 1] + self.grid[i + 1][j]

                elif i == self.N - 1 and j == 0:
                    # check for the most bottom left cell
                    neighborhoodSum = self.grid[i - 1][j] + self.grid[i][j + 1] + self.grid[i - 1][j + 1]

                elif i == self.N - 1 and j == self.N - 1:
                    # check for the most bottom right cell
                    neighborhoodSum = self.grid[i - 1][j] + self.grid[i][j - 1] + self.grid[i - 1][j - 1]

                elif i == self.N - 1 and self.N > j > 0:
                    # check for the bottom row of cells except from the most right and left cells
                    neighborhoodSum = self.grid[i][j - 1] + self.grid[i][j + 1] + self.grid[i - 1][j] + \
                                      self.grid[i - 1][j - 1] + self.grid[i - 1][j + 1]

                elif self.N - 1 > i > 0 and j == 0:
                    # check for the left row of cells except from the most top and bottom cells
                    neighborhoodSum = self.grid[i - 1][j] + self.grid[i + 1][j] + self.grid[i][j + 1] + \
                                      self.grid[i - 1][j + 1] + self.grid[i + 1][j + 1]

                elif 0 < i < self.N - 1 == j:
                    # check for the right row of cells except from the most top and bottom cells
                    neighborhoodSum = self.grid[i - 1][j] + self.grid[i + 1][j] + self.grid[i][j - 1] + \
                                      self.grid[i - 1][j - 1] + self.grid[i + 1][j - 1]

                else:
                    # check for any cells except from those special conditions above
                    neighborhoodSum = self.grid[i][j - 1] + self.grid[i][j + 1] + self.grid[i - 1][j] + \
                                      self.grid[i + 1][j] + self.grid[i - 1][j - 1] + self.grid[i - 1][j + 1] + \
                                      self.grid[i + 1][j - 1] + self.grid[i + 1][j + 1]
                new_grid[i][j] = neighborhoodSum     
        """
        # implement the GoL rules by thresholding the weights

        # PART A CODE HERE
        # update the grid
        """
        for i in range(self.N):
            for j in range(self.N):
                if self.grid[i][j] == self.aliveValue:
                    # Any live cell with more than three or fewer than 2 live neighbors dies, as if by overpopulation.
                    if new_grid[i][j] < 2 or new_grid[i][j] > 3:
                        self.grid[i][j] = self.deadValue
                else:
                    if new_grid[i][j] == 3:
                        # Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction
                        self.grid[i][j] = self.aliveValue
        """

        # PART B
        # do a matrix dot product with grid and neighborhood grid to calculate the convolution

        # using "same" to calculate the convolution when the anchor
        # of the filter coincides with the corner of the image
        # using "fill" to avoid miscalculate the edge of filter that out of the edge of image (calculate as 0)

        new_grid = signal.convolve2d(self.grid, self.neighborhood, 'same', 'fill')

        # use logical gate instead of for loop to apply the four rules in order to improve the algorithm

        # 1. set cells whose alive and neighbors equal to 2 to keep alive using the logical AND gate
        # 2. set cells whose alive or dead and neighbors equal to 3 to become alive or keep alive using
        #    the logical OR gate
        # 3. set any cells to 0 to make it dead because of the concept of logical gate
        # 4. update the grid
        self.grid = np.logical_or(np.logical_and(self.grid, np.equal(new_grid, 2)), np.equal(new_grid, 3))

    def insertBlinker(self, index=(0, 0)):
        '''
        Insert a blinker oscillator construct at the index position
        '''
        self.grid[index[0], index[1] + 1] = self.aliveValue
        self.grid[index[0] + 1, index[1] + 1] = self.aliveValue
        self.grid[index[0] + 2, index[1] + 1] = self.aliveValue

    def insertGlider(self, index=(0, 0)):
        '''
        Insert a glider construct at the index position
        '''
        self.grid[index[0], index[1] + 1] = self.aliveValue
        self.grid[index[0] + 1, index[1] + 2] = self.aliveValue
        self.grid[index[0] + 2, index[1]] = self.aliveValue
        self.grid[index[0] + 2, index[1] + 1] = self.aliveValue
        self.grid[index[0] + 2, index[1] + 2] = self.aliveValue

    def insertGliderGun(self, index=(0, 0)):
        '''
        Insert a glider construct at the index position
        '''
        self.grid[index[0] + 1, index[1] + 25] = self.aliveValue

        self.grid[index[0] + 2, index[1] + 23] = self.aliveValue
        self.grid[index[0] + 2, index[1] + 25] = self.aliveValue

        self.grid[index[0] + 3, index[1] + 13] = self.aliveValue
        self.grid[index[0] + 3, index[1] + 14] = self.aliveValue
        self.grid[index[0] + 3, index[1] + 21] = self.aliveValue
        self.grid[index[0] + 3, index[1] + 22] = self.aliveValue
        self.grid[index[0] + 3, index[1] + 35] = self.aliveValue
        self.grid[index[0] + 3, index[1] + 36] = self.aliveValue

        self.grid[index[0] + 4, index[1] + 12] = self.aliveValue
        self.grid[index[0] + 4, index[1] + 16] = self.aliveValue
        self.grid[index[0] + 4, index[1] + 21] = self.aliveValue
        self.grid[index[0] + 4, index[1] + 22] = self.aliveValue
        self.grid[index[0] + 4, index[1] + 35] = self.aliveValue
        self.grid[index[0] + 4, index[1] + 36] = self.aliveValue

        self.grid[index[0] + 5, index[1] + 1] = self.aliveValue
        self.grid[index[0] + 5, index[1] + 2] = self.aliveValue
        self.grid[index[0] + 5, index[1] + 11] = self.aliveValue
        self.grid[index[0] + 5, index[1] + 17] = self.aliveValue
        self.grid[index[0] + 5, index[1] + 21] = self.aliveValue
        self.grid[index[0] + 5, index[1] + 22] = self.aliveValue

        self.grid[index[0] + 6, index[1] + 1] = self.aliveValue
        self.grid[index[0] + 6, index[1] + 2] = self.aliveValue
        self.grid[index[0] + 6, index[1] + 11] = self.aliveValue
        self.grid[index[0] + 6, index[1] + 15] = self.aliveValue
        self.grid[index[0] + 6, index[1] + 17] = self.aliveValue
        self.grid[index[0] + 6, index[1] + 17] = self.aliveValue
        self.grid[index[0] + 6, index[1] + 18] = self.aliveValue
        self.grid[index[0] + 6, index[1] + 23] = self.aliveValue
        self.grid[index[0] + 6, index[1] + 25] = self.aliveValue

        self.grid[index[0] + 7, index[1] + 11] = self.aliveValue
        self.grid[index[0] + 7, index[1] + 17] = self.aliveValue
        self.grid[index[0] + 7, index[1] + 25] = self.aliveValue

        self.grid[index[0] + 8, index[1] + 12] = self.aliveValue
        self.grid[index[0] + 8, index[1] + 16] = self.aliveValue

        self.grid[index[0] + 9, index[1] + 13] = self.aliveValue
        self.grid[index[0] + 9, index[1] + 14] = self.aliveValue

    def insertFromPlainText(self, txtString, pad=0):
        '''
        Assumes txtString contains the entire pattern as a human readable pattern without comments
        '''
        listTxtString = txtString.split("\n")
        for i in range(len(listTxtString)):
            listLine = list(listTxtString[i])
            for j in range(len(listLine)):
                if listLine[j] == 'O':
                    self.grid[i+10][j+10] = self.aliveValue
                else:
                    self.grid[i+10][j+10] = self.deadValue

    def insertFromRLE(self, rleString, pad=0):
        '''
        Given string loaded from RLE file, populate the game grid
        '''
        rle_process = rle.RunLengthEncodedParser(rleString)
        lines_contents = rle_process.human_friendly_pattern
        # print(lines_contents)
        listRleString = lines_contents.split("\n")
        for i in range(len(listRleString)):
            listLine = list(listRleString[i])
            for j in range(len(listLine)):
                if listLine[j] == 'o':
                    self.grid[i][j] = self.aliveValue
                else:
                    self.grid[i][j] = self.deadValue
